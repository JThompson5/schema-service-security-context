import { Loggable } from '@managebuy/logging-utils';

export class ServiceSecurityContext {
  @Loggable()
  permissionAttributes: Record<string, string>;
}